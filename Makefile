dependancies:
	echo "Script done for Ubuntu"
	# Check for the installation of dia, texlive and python : if they are up to date this will be fine ;) else... it may take long 
	sudo apt update
	sudo apt install dia texlive python evince

install:
	echo "Local installation as alias in bashrc"
	# Will append to your .bashrc file dianausaur alias as diano to be called from everywhere inside your session
	echo "alias diano='python "$(realpath dianausaur.py)"'">>~/.bashrc
	# Source the bashrc
	source ~/.bashrc
