#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sans titre.py
#
#  Copyright 2019 Justin Cano
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


from shutil import copyfile
import os, sys, glob
from re import search

reload(sys)
sys.setdefaultencoding('utf-8')
# Regenerate all directory (linux CLI)
print("Generate all .tex automatically [uncomment for non UNIX users]")
os.system("dia *.dia -t tex")
# Input file
print("Availiable files list, ordered by date")
#s = os.system("ls *tex | grep -v _diano")
filelist = []
# Getting file tex list
filelist1 = glob.glob("*tex")
# Sort by date
filelist1.sort(key=os.path.getmtime)
# Reverse the list (the young First!)
filelist1.reverse()
# Remove autogenered tempfiles
filelist = []
for f in filelist1:
    print(f)
    if '_diano' not in f :
        filelist.append(f)
print(filelist)
# Length of the file list
N = len(filelist)
# Print file list
for i in range(N):
    s = str(i+1) + " -- " + filelist[i]
    print(s)
# Dumb test to avoid out of list errors...
w = True
while w:
    number_selected = int(raw_input('Type the number associated to the file \n'))
    if N < number_selected:
        print("Indice out of the list! Try again...")
    else:
        w = False
inputfile=filelist[number_selected-1].replace('.tex','')
#First create a copy of the current file to be built then
sauroutput = inputfile+"_diano"
copyfile(inputfile+".tex",sauroutput+".tex")
# We will work on this file
f = open(sauroutput+".tex","rb")
# Latex preamble package and begin document
preamble = """
% This autogen diacode has been prouly modified by a Python script
% written by Justin Cano, Polytechnique Montreal. dianausor allows you to
%compile in pdf with accurate formulaes in cli your .tex dia generated
% TiKZ code.
% Standalone class used with classical parameters
\\documentclass[16pt]{standalone}
\\usepackage[utf8]{inputenc}
\\usepackage{tikz}
\\usepackage{amsmath}
\\begin{document}
% Add your personnal macros here :
\\newcommand{\mbf}{\mathbf} % I am so lazy, I prefer three letters than 5 ;)
"""
footer = """
\\end{document}
"""
# We read the lines of our document
lines = f.readlines()
lines.insert(len(lines),footer)
lines.insert(0,preamble)

f.close()
# We will write our changes
f = open(sauroutput+".tex","wb")
f.writelines(lines)
f.close()

s = open(sauroutput+".tex").read()
# Formulas handling
s = s.replace('\\$','$')
s = s.replace('\\_','_')
s = s.replace('\\^{}','^')
s = s.replace('\\ensuremath{\\backslash}','\\')
s = s.replace('\{','{')
s = s.replace('\}','}')
f = open(sauroutput+".tex", 'w')
f.write(s)
f.close()

# Latex compilation
compiling_command = "pdflatex "+sauroutput + ".tex"
os.system(compiling_command)
# Cleaning intermediate output
extensions = ["aux","bbl","blg","brf","idx","ilg","ind","lof","log","lol","lot","out","toc","synctex.gz"]
for ext in extensions:
	os.system("rm -f *"+ext)
# Pdf labeled as output and preview
os.system("mv *diano.pdf output.pdf")
os.system("evince output.pdf")
